###
# Copyright (c) 2015, urnan
# All rights reserved.
#
#
###

import time
import string
import random
import supybot.ircmsgs as ircmsgs
import supybot.world as world
import supybot.schedule as schedule
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.conf as conf
import supybot.ircdb as ircdb
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('Roulette')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


class Roulette(callbacks.Plugin):
    """shoot motherfuckers"""
    pass

    def shoot(self, irc, msg, args, channel, victim):
        """[<channel>] [<nick>]

        Force your victim into a game of Russian Roulette.
        """
        channel = msg.args[0]
        channel = ircutils.toLower(channel)
        if 'Q' in irc.state.channels[channel].modes:
            s="Not while Q is set. (Don't try to kick people during games.)"
            irc.reply(s)
            return
        if victim == None:
            victim = msg.nick
        victim = string.lower(victim)
        if not self.registryValue('AllowShots', msg.args[0]):
            irc.noReply()
            return
        if irc.nick in victim:
            irc.reply('Why would I do that?')
            return
        if string.lower('papapuff') in victim and msg.nick != 'PapaPuff':
            irc.reply('urnan')
            return
	if string.lower('zippy') in victim and msg.nick != 'zippy':
	    irc.reply('urnan')
	    return
        if string.lower('bofh') in victim and msg.nick != 'PapaPuff':
            irc.sendMsg(ircmsgs.kick(channel, msg.nick, '*Hit by train*'))
            return
        if string.lower('luna') in victim:
            irc.sendMsg(ircmsgs.kick(channel, msg.nick, 'NO'))
            return
        found = False
        for nick in list(irc.state.channels[channel].users):
            if victim == string.lower(nick):
                victim = nick
                found = True
        if not found:
            irc.reply('I can\'t shoot someone who isn\'t here, dumbass.')
            return
        bullets = ('blank', 'blank', 'blank', 'kick', 'kick', 'ban')
        shot = random.choice(bullets)
        spin = ('loads his Smith & Wesson 500, spins the cylinder and points it at %s' % victim)
        if shot == 'blank':
            irc.sendMsg(ircmsgs.action(channel, spin))
            irc.sendMsg(ircmsgs.privmsg(channel, '*Click* You lucky motherfucker.'))
            return
        if shot == 'kick':
            irc.sendMsg(ircmsgs.action(channel, spin))
            irc.sendMsg(ircmsgs.kick(channel, victim, 'BANG!'))
            return
        if shot == 'ban':
            minban = self.registryValue('MinBanTime', msg.args[0])
            maxban = self.registryValue('MaxBanTime', msg.args[0])
            bantime = random.randrange(minban, maxban)
            bannedHostmask = irc.state.nickToHostmask(victim)
            banmaskstyle = conf.supybot.protocols.irc.banmask
            banmask = banmaskstyle.makeBanmask(bannedHostmask)
            irc.sendMsg(ircmsgs.action(channel, spin))
            unban = (irc.sendMsg(ircmsgs.unban(channel, banmask)))
            irc.sendMsg(ircmsgs.ban(channel, banmask))
            t = time.time() + bantime
            irc.sendMsg(ircmsgs.kick(channel, victim, 'BANG! Respawn in %s seconds.' % bantime))
            def f():
                irc.queueMsg(ircmsgs.unban(channel, banmask))
            schedule.addEvent(f, t)
            return

    shoot = wrap(shoot, ['Channel', optional('somethingWithoutSpaces')])
Class = Roulette


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
