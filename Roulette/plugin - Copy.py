###
# Copyright (c) 2015, urnan
# All rights reserved.
#
#
###

import time
import string
import random
import supybot.ircmsgs as ircmsgs
import supybot.world as world
import supybot.schedule as schedule
import supybot.utils as utils
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.conf as conf
import supybot.ircdb as ircdb
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('Roulette')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


class Roulette(callbacks.Plugin):
    """shoot motherfuckers"""
    pass

    def shoot(self, irc, msg, args, channel, nick):
        """[<channel>] [<nick>]

        Force your victim into a game of Russian Roulette.
        """
        channel = msg.args[0]
        if irc.nick in nick:
            irc.reply('Why would I do that?')
            return
        #if not nick in list(irc.state.channels.[channel].users):
        #    irc.reply('I cannot shoot someone who isn\'t here.')
        #    break
        bullets = ('blank', 'blank', 'blank', 'kick', 'kick', 'ban')
        shot = random.choice(bullets)
        spin = ('loads his Smith & Wesson 500, spins the cylinder and points it at %s' % nick)
        if shot == 'blank':
            irc.sendMsg(ircmsgs.action(channel, spin))
            irc.sendMsg(ircmsgs.privmsg(channel, '*Click* You lucky motherfucker.'))
            return
        if shot == 'kick':
            irc.sendMsg(ircmsgs.action(channel, spin))
            irc.sendMsg(ircmsgs.kick(channel, nick, 'BANG!'))
            return
        if shot == 'ban':
            minban = 30
            maxban = 60
            bantime = random.randrange(minban, maxban)
            bannedHostmask = irc.state.nickToHostmask(nick)
            banmaskstyle = conf.supybot.protocols.irc.banmask
            banmask = banmaskstyle.makeBanmask(bannedHostmask)
            irc.sendMsg(ircmsgs.action(channel, spin))
            unban = (irc.sendMsg(ircmsgs.unban(channel, banmask)))
            irc.sendMsg(ircmsgs.ban(channel, banmask))
            t = time.time() + bantime
            irc.sendMsg(ircmsgs.kick(channel, nick, 'BANG! Respawn in %s seconds.' % bantime))
            def f():
                irc.queueMsg(ircmsgs.unban(channel, banmask))
            schedule.addEvent(f, t)
            return

    shoot = wrap(shoot, ['Channel', ('somethingWithoutSpaces')])
Class = Roulette


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
