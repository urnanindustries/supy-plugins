# -*- coding: utf-8 -*-
###
# Copyright (c) 2015, urnan
# All rights reserved.
#
#
###

import time
import string
import random
import supybot.utils as utils
import supybot.world as world
from supybot.commands import *
import supybot.plugins as plugins
import supybot.ircmsgs as ircmsgs
import supybot.ircutils as ircutils
import supybot.schedule as schedule
import supybot.callbacks as callbacks
import supybot.ircdb as ircdb


class Grenade(callbacks.Plugin):
    """Hot potato game"""
    threaded = True
    
    def __init__(self, irc):
        self.__parent = super(Grenade, self)
        self.__parent.__init__(irc)
        self.rng = random.Random()
        self.rng.seed()
        self.bombs = {}


    class Bomb():
        def __init__(self, irc, victim, detonateTime, channel):
            self.victim = victim
            self.detonateTime = detonateTime
            self.active = True
            self.channel = channel
            self.irc = irc
            self.rng = random.Random()
            self.rng.seed()
            def boom():
                self.boom(irc)
            schedule.addEvent(boom, time.time() + self.detonateTime, '%s_bomb' % self.channel)
            s = 'pulls the pin out of a grenade and throws it at %s.' % (victim)
            self.irc.sendMsg(ircmsgs.action(self.channel, s))
           
        def throw(self, irc, newVictim):
            self.newVictim = newVictim
            newVictim = string.lower(newVictim)    
            q = 'The grenade is quickly thrown at %s.' % (newVictim)
            self.irc.queueMsg(ircmsgs.privmsg(self.channel, q))
            self.victim = self.newVictim
    
        def boom(self, irc):
            self.active = False
            self.irc.sendMsg(ircmsgs.kick(self.channel, self.victim, 'BOOOOOOOOOOOOOOOOM!'))

        def dud(self, irc, channel):
            self.active = False
            schedule.removeEvent('%s_bomb' % channel)
            s = 'snatches back the grenade and sits on it.'
            self.irc.sendMsg(ircmsgs.action(channel, s))
            
    def grenade(self, irc, msg, args, channel, victim):
        """[<Victim>]

        Start the grenade game (hot potato)"""
        channel = ircutils.toLower(channel)
        self.victim = victim
        if 'Q' in irc.state.channels[channel].modes:
            s="Not while Q is set. (Don't try to kick people during games.)"
            irc.reply(s)
            return
        if not self.registryValue('allowGrenade', msg.args[0]):
            irc.reply('Grenades aren\'t allowed in here.')
            return
        try:
            if self.bombs[channel].active:
                irc.reply('One active grenade is enough, I think.')
                return
        except KeyError:
            pass
        nicks = list(irc.state.channels[channel].users)
        randnick = self.rng.choice(nicks)
        if victim == None:
            if self.registryValue('allowRandom', msg.args[0]):
                victim = randnick
            else:
                victim = msg.nick
                pass
        victim = string.lower(victim)
        found = False
        for nick in list(irc.state.channels[channel].users):
            if victim == string.lower(nick):
                victim = nick
                found = True
        if not found:
            irc.reply(('Who is %s?') % (victim))
            return
        if victim == irc.nick:
            irc.reply('Nice try, I can be mean too.')
            victim = msg.nick
        detonateTime = self.rng.randint(self.registryValue('minTime', msg.args[0]), self.registryValue('maxTime', msg.args[0]))
        self.bombs[channel] = self.Bomb(irc, victim, detonateTime, channel)                          
    grenade = wrap(grenade, ['Channel', optional('something')])

            
    def throw(self, irc, msg, args, channel, newVictim):
        """<Victim>

        Will throw the grenade to <Victim>""" 
        channel = ircutils.toLower(channel)
        try:
            if not self.bombs[channel].active:
                return
            if not ircutils.nickEqual(self.bombs[channel].victim, msg.nick) and msg.nick != 'zippy':
                irc.reply('But you don\'t have the grenade...')
                return
            newVictim = string.lower(newVictim)
            found = False
            for nick in list(irc.state.channels[channel].users):
                if newVictim == string.lower(nick):
                    victim = nick
                    found = True
            if not found:
                irc.reply(('Who is %s?') % (newVictim))
                return
            if newVictim == irc.nick and msg.nick != 'PapaPuff':
                s = 'kicks the grenade straight back at %s' % (msg.nick)
                irc.reply(s, action=True)
                return
            self.bombs[channel].throw(irc, newVictim)
        except KeyError:
            pass
        irc.noReply()
    throw = wrap(throw, ['Channel', 'something'])

    def dud(self, irc, msg, args):
        """(takes no arguments)

        Will cancel the grenade"""
        channel = msg.args[0]
        try:
            if not ircdb.checkCapability(msg.nick, 'admin'):
                irc.noReply()
                return
            if not self.bombs[channel].active:
                irc.reply('No active grenades.')
                return
            else:
                self.bombs[channel].dud(irc, channel)
        except KeyError:
            pass
        irc.noReply()
    dud = wrap(dud)

                           
Class = Grenade


# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
