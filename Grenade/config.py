###
# Copyright (c) 2015, urnan
# All rights reserved.
#
#
###

import supybot.conf as conf
import supybot.registry as registry
try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('Grenade')
except:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


def configure(advanced):
    # This will be called by supybot to configure this module.  advanced is
    # a bool that specifies whether the user identified themself as an advanced
    # user or not.  You should effect your configuration by manipulating the
    # registry as appropriate.
    from supybot.questions import expect, anything, something, yn
    conf.registerPlugin('Grenade', True)


Grenade = conf.registerPlugin('Grenade')
# This is where your configuration variables (if any) should go.  For example:
# conf.registerGlobalValue(Grenade, 'someConfigVariableName',
#     registry.Boolean(False, _("""Help for someConfigVariableName.""")))
conf.registerChannelValue(Grenade, 'AllowGrenade',
    registry.Boolean(False, """Determines whether grenades are allowed
        in the channel"""))

conf.registerChannelValue(Grenade, 'AllowRandom',
    registry.Boolean(False, """Determines whether random users will get grenaded
        in the channel"""))

conf.registerChannelValue(Grenade, 'minTime',
    registry.PositiveInteger(15, """Determines the minimum time before
        the grenade detonates"""))

conf.registerChannelValue(Grenade, 'maxTime',
    registry.PositiveInteger(60, """Determines the maximum time before
        the grenade detonates"""))

# vim:set shiftwidth=4 tabstop=4 expandtab textwidth=79:
