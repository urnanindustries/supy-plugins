###
# Copyright (c) 2015, urnan
# All rights reserved.
#
#
###

import re
import random

import supybot.conf as conf
import supybot.utils as utils
from supybot.commands import *
import supybot.ircmsgs as ircmsgs
import supybot.ircutils as ircutils
import supybot.callbacks as callbacks
import supybot.ircdb as ircdb
import time
import os, errno
import pickle

try:
    from supybot.i18n import PluginInternationalization
    _ = PluginInternationalization('CAH')
except ImportError:
    # Placeholder that allows to run the plugin on a bot
    # without the i18n module
    _ = lambda x: x


class CAH(callbacks.Plugin):
    """Game of Cards Against Humanity"""

    game=[{},{},{},{},{}]

    channeloptions = {}
    channeloptions['allow_game']=False
    channeloptions['nplayers']=10
    channeloptions['defrounds'] = 5

    #def make_sure_path_exists(path):
    #    try:
    #        os.makedirs(path)
    #    except OSError as exception:
    #        if exception.errno != errno.EEXIST:
    #           raise

    #make_sure_path_exists(r'%s%scah' %s (conf.supybot.directories.data(),os.sep))
    dataPath=r'%s%scah%s' % (conf.supybot.directories.data(),os.sep,os.sep)
    prefixChar = conf.supybot.reply.whenAddressedBy.chars()[0]

    #NEW the following is to to setup the score file
    def __init__(self, irc):
        self.__parent = super(CAH, self)
        self.__parent.__init__(irc)
        self.scores = {}
        self.scorefile = "/home/munchies/bot/plugins/CAH/scores.txt"
        if not os.path.exists(self.scorefile):
            f = open(self.scorefile, 'w')
            f.close()
        f = open(self.scorefile, 'r')
        line = f.readline()
        while line:
            (name, score) = line.split(' ')
            self.scores[name] = int(score.strip('\r\n'))
            line = f.readline()
        f.close()
      
    def start(self, irc, msg, args,):
        """

        Starts a new game of Cards Against Humanity
        """
        try:
            self._read_options(irc)
        except:
            pass
        #if self.channeloptions['allow_game']==False:
        #    irc.noreply()
        #    return
        nick = msg.nick
        table = self._gettablefromnick(nick)
        if table!=None:
            irc.reply('You are already in a game.', prefixNick=False)
            return
        if not self.registryValue('allow', msg.args[0]):
            irc.reply('Not in here.', prefixNick=False)
            return
        table=self._getopentable()
        if table==None:
            irc.reply('There are no available games to join right now.', prefixNick=False)
            return

        existing = False
        tables = self._getcurrenttables()
        channel = msg.args[0]
        for t in tables:
            if self.game[t]['channel'] == msg.args[0]:
                existing = True

        if existing == True:
            irc.sendMsg("There's already a game running in this channel.", prefixNick=False, to=msg.args[0])
            return

        prefixChar = conf.supybot.reply.whenAddressedBy.chars()[0]
        self._cleanup(table)
        self.game[table]={}
        self.game[table]['channel']=msg.args[0]
        self.game[table]['players']=[]
        self.game[table]['pdealers']=[]
        self.game[table]['queue']=[]
        self.game[table]['waiting']=False
        self.game[table]['nplayers']=int(self.channeloptions['nplayers'])
        self._cleanup(table)
        self.game[table]['players'].append(nick)
        irc.reply('%s has started a new game of Cards Against Humanity. To join in, type "%scah join".' % (nick, prefixChar), prefixNick=False)
        self.game[table]['phase']='join'
        cards = [line.strip() for line in open("/home/munchies/bot/plugins/CAH/whitecards.txt", 'r')]
        self.game[table]['deck']=[]
        for card in cards:
            self.game[table]['deck'].append(card)
        cards = [line.strip() for line in open("/home/munchies/bot/plugins/CAH/blackcards.txt", 'r')]
        self.game[table]['blackdeck']=[]
        for card in cards:
            self.game[table]['blackdeck'].append(card)

    start = wrap(start)

    def begin(self, irc, msg, args, text):
        """[<rounds>]
        
        Begin the game, optionally define number of rounds
        """
        if text:
            rounds = text
        else:
            rounds = self.channeloptions['defrounds']

        nick=msg.nick
        table=self._gettablefromnick(nick)
        if table==None:
            irc.reply('You did not join.', prefixNick=False)
            return
        if self.game[table]['phase']!='join':
            irc.reply('You cannot do this right now.', prefixNick=False)
            return
        nplayers=len(self.game[table]['players'])
        if nplayers<3:
            irc.reply('You need at least 3 players.', prefixNick=False)
            return 
        self.game[table]['rounds'] = rounds
        self.game[table]['round']=0
        irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["+Q"]))
        self._cah_begin(irc, table)
 
    begin = wrap(begin, ['public', optional('something')])

    def _cah_begin(self, irc, table):
        """
        urnan
        """
        self._cleanup(table)
        players=self.game[table]['players']
        pastdealers=self.game[table]['pdealers']
        newdealer=[p for p in players if p not in pastdealers]
        self.game[table]['dealer']=random.choice(newdealer)
        dealer = self.game[table]['dealer']
        self.game[table]['pdealers'].append(dealer)
        if len(pastdealers) >= len(players):
            self.game[table]['pdealers']=[]
        whiteplayers = [p for p in players if p!=dealer]
        for n in whiteplayers:
            self.game[table]['whiteplayers'][n]={}
        self.game[table]['blackcards']=[]
        random.shuffle(self.game[table]['blackdeck'])
        random.shuffle(self.game[table]['deck'])
        for i in range(0,5):
            card=self.game[table]['blackdeck'].pop(random.randint(0,len(self.game[table]['blackdeck'])-1))
            self.game[table]['blackcards'].append(card)
            try:
                self.game[table]['blackdeck'].remove(card)
            except:
                pass
        for n in self.game[table]['whiteplayers']:
            self.game[table]['whiteplayers'][n]['hand']=[]
            for i in range(0,5):
                card=self.game[table]['deck'].pop(random.randint(0,len(self.game[table]['deck'])-1))
                self.game[table]['whiteplayers'][n]['hand'].append(card)
                try:
                    self.game[table]['deck'].remove(card)
                except:
                    pass

        self.game[table]['phase']='running'
        s ='Round %s. %s must choose a question and the rest will have to choose an answer afterwards.' % (self.game[table]['round']+1, self.game[table]['dealer'])
        irc.reply(s, prefixNick=False, to=self.game[table]['channel'])
        self.game[table]['turn']='dealer'
        self._question(irc, table)

    def choose(self, irc, msg, args, text):
        """
        Pick a card
        """
        nick = msg.nick
        table=self._gettablefromnick(nick)
        nick=msg.nick
        if table==None:
            irc.reply('You are not playing.', to=nick, private=True)
            return
        if text < 1:
            irc.reply('Are you dumb or retarded?', prefixNick=False)
            return
        dealer=self.game[table]['dealer']
        players = self.game[table]['whiteplayers'].keys()
        if nick in dealer:
            if self.game[table]['turn']=='dealer':
                try:
                    indo = text-1
                    indy = self.game[table]['blackcards'][indo]
                    self.game[table]['question']=indy
                    self.game[table]['turn']='tellstatus'
                    s = 'The question is:\x02\x034 %s' % self.game[table]['question']
                    irc.reply(s, prefixNick=False, to=self.game[table]['channel'])
                    self._tellstatus(irc, table)
                except IndexError:
                    irc.reply('That is not a valid choice.', prefixNick=False)   

            elif self.game[table]['turn']=='picked':
                try:
                    indo = text-1
                    indy = self.game[table]['answerstwo'][indo]
                    winner = indy
                    self._winner(irc, table, winner)
                except IndexError:
                    irc.reply('That is not a valid choice.', prefixNick=False)   

            else:
                irc.reply('Not your time to choose.', to=nick, private=True)
                return

        elif nick in players:
            if self.game[table]['turn']!='tellstatus':
                irc.reply('You cannot do that right now.', to=nick, private=True)
                return
            if nick in self.game[table]['picked']:
                irc.reply('You have already picked an answer.', to=nick, private=True)
                return
            else:
                try:
                    indo = text-1
                    indy = self.game[table]['whiteplayers'][nick]['hand'][indo]
                    self.game[table]['answers'].update({nick: indy})
                    self.game[table]['answerstwo'].append(indy)
                    self.game[table]['whiteplayers'][nick]['choice'] = indy
                    self.game[table]['picked'].append(nick)
                    s= '%s has chosen an answer.' % nick
                    irc.reply(s, prefixNick=False, to=self.game[table]['channel'])
                    if len(self.game[table]['whiteplayers'].keys()) == len(self.game[table]['picked']):
                        random.shuffle(self.game[table]['answerstwo'])
                        self._picked(irc, table)
                except IndexError:
                    irc.reply('That is not a valid choice.', prefixNick=False)   
        else:
            irc.reply('But you\'re not playing.', prefixNick=False)

    choose = wrap(choose, ['int'])

    def join(self, irc, msg, args, table, nick):
        """[<table>]

        Join a game of Cards Against Humanity
        """
        try:
            self._read_options(irc)
        except:
            pass
        #if self.channeloptions['allow_game']==False:
        #    irc.noreply()
        #    return

        nick=msg.nick
        if table !=None: table-=1
        tables=self._getcurrenttables()
        if not tables:
            irc.reply('There are no games to join.', prefixNick=False)
            return
        if table !=None and table not in tables:
            if table not in range(len(self.game)):
                irc.reply('That table doesn\'t exist.', prefixNick=False)
                return
            irc.reply('There is no game at that table.', prefixNick=False)
            return
        tables=[t for t in tables if self.game[t]['channel']==msg.args[0]]
        if table != None:
            if table not in tables:
                irc.reply('That table is in another channel.', prefixNick=False)
                return
            tables=[table]
        if len(tables)==0:
            irc.reply('There are no games in this channel.', prefixNick=False)
            return
        elif len(tables)==1:
            table=tables[0]       
        if self.game[table]['phase']=='join':
            if nick in self.game[table]['players']:
                irc.reply('You have already joined.', prefixNick=False)
                return
            elif len(self.game[table]['players']) < self.game[table]['nplayers']:
                self.game[table]['players'].append(nick)
                irc.reply('%s has joined the game.' % nick, prefixNick=False)
                return
        else:
            if self.game[table]['phase']=='running':
                if self.game[table]['turn']=='dealer':
                    self.game[table]['players'].append(nick)
                    self.game[table]['whiteplayers'][nick]={}
                    self.game[table]['whiteplayers'][nick]['hand']=[]
                    for i in range(0,5):
                        card=self.game[table]['deck'].pop(random.randint(0,len(self.game[table]['deck'])-1))
                        self.game[table]['whiteplayers'][nick]['hand'].append(card)
                    irc.reply('%s has joined the game.' % nick, prefixNick=False)
                    return
                else:
                    self.game[table]['queue'].append(nick)
                    self.game[table]['waiting']=True
                    irc.reply('%s will be joining the game next round.' % nick, prefixNick=False)
                    return
            elif self.game[table]['phase']=='':
                irc.reply('No games to join. Start one.', prefixNick=False)
                return
            else:
                irc.reply('No.', prefixNick=False)
                return
    
    join = wrap(join, ['public', optional('int'), optional('something')])


    def tellstatus(self, irc, msg, args):
        """
        tellstatus
        """
        nick=msg.nick
        table=self._gettablefromnick(nick)
        if table == None:
            return
        if self.game[table]['turn']=='dealer':
            if nick == self.game[table]['dealer']:
                self._question(irc, table)
            else:
                irc.reply('Wait for the question.', to=nick, private=True)
                return
        elif self.game[table]['turn']=='tellstatus':
            if nick != self.game[table]['dealer']:
                cards = self.game[table]['whiteplayers'][nick]['hand']
                txt='\x02'
                for c in cards:
                    indy = (self.game[table]['whiteplayers'][nick]['hand'].index(c))+1
                    txt += '\x039 %s:\x034 %s ' % (indy, c)
                    txt=txt.rsplit(' ',1)[0]+''
                irc.reply(txt, to=nick, private=True)

            else:
                irc.reply('Wait for everyone to pick their answer.', to=nick, private=True)
                return
        elif self.game[table]['turn']=='picked':
            if nick == self.game[table]['dealer']:
                self._picked(irc, table)
            else:
                irc.reply('You are done for this round.', prefixNick=False)
                return

    def writeScores(self):
        f = open('/home/munchies/bot/plugins/CAH/scores.txt', 'w')
        scores = self.scores.iteritems()
        for i in range(0, len(self.scores)):
            score = scores.next()
            f.write('%s %s\n' % (score[0], score[1]))
        f.close()

    def _winner(self, irc, table, winner):
        """
        urnan
        """
        
        s = '\x02\x034%s\034 \x039%s\039' % (self.game[table]['question'], winner)
        irc.reply(s, prefixNick=False, to=self.game[table]['channel'])
        for w in self.game[table]['whiteplayers'].keys():
            if winner in self.game[table]['whiteplayers'][w]['choice']:
                winningplayer = w
        if not winningplayer in self.scores:
            self.scores[winningplayer]=0
        self.scores[winningplayer] += 1
        self.writeScores()                
        s = 'Round goes to \x02%s. \x033 Wins: \x034 %d' % (winningplayer, self.scores[winningplayer])
        irc.reply(s, prefixNick=False, to=self.game[table]['channel'])
        txt='Other answers:'
        players=self.game[table]['whiteplayers'].keys()
        for p in players:
            if p != winningplayer:
                ochoice=self.game[table]['whiteplayers'][p]['choice']
                txt += '\x039 %s:\x034 %s ' % (p, ochoice)
                txt=txt.rsplit(' ',1)[0]+''
        irc.reply(txt, prefixNick=False, to=self.game[table]['channel'])
        

        r = self.game[table]['round']
        self.game[table]['round'] = r+1
        if int(self.game[table]['round']) == int(self.game[table]['rounds']):
            s = '\x02Game over.'
            irc.reply(s, prefixNick=False, to=self.game[table]['channel'])
            irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
            self.game[table]={}
            self.game[table]['players']=[]
            self.game[table]['phase']=''
        else:
            if self.game[table]['waiting']:
                for n in self.game[table]['queue']:
                    self.game[table]['players'].append(n)
                    self.game[table]['queue']=[]
                self.game[table]['waiting']=False
            self._cleanup(table)
            self._cah_begin(irc, table)
   

    def _picked(self, irc, table):
        """
        urnan
        """
        dealer = self.game[table]['dealer']
        s = 'All players have picked an answer. %s must now choose their favourite answer.' % dealer
        irc.reply(s, prefixNick=False, to=self.game[table]['channel'])
        self.game[table]['turn']='picked'
        s = 'Your question was: \x02\x034%s\02\034 Here are the provided answers. Use %schoose number to select your answer.' % (self.game[table]['question'], conf.supybot.reply.whenAddressedBy.chars()[0])
        irc.reply(s, to=dealer, private=True)
        txt='\x02'
        for n in self.game[table]['answerstwo']:
            indy = (self.game[table]['answerstwo'].index(n))+1
            txt += '\x039 %s:\x034 %s ' % (indy, n)
        txt=txt.rsplit(' ',1)[0]+''
        irc.reply(txt, to=dealer, private=True)
        

    def _question(self, irc, table):
        """
        urnan
        """
        dealer = self.game[table]['dealer']
        s = 'Here are the available questions. Use "%schoose number" to select the question.' % conf.supybot.reply.whenAddressedBy.chars()[0]
        irc.reply(s, to=dealer, private=True)
        txt='\x02'
        for c in self.game[table]['blackcards']:
            indy = (self.game[table]['blackcards'].index(c))+1
            txt+='\x039 %s:\x034 %s ' % (indy, c)
        txt=txt.rsplit(' ',1)[0]+''
        irc.reply(txt, to=dealer, private=True)


    def _cards(self, irc, table, n):
        """
        urnan
        """
        cards = self.game[table]['whiteplayers'][n]['hand']
        txt='\x02'
        for c in cards:
            indy = (self.game[table]['whiteplayers'][n]['hand'].index(c))+1
            txt += '\x039 %s:\x034 %s ' % (indy, c)
        txt=txt.rsplit(' ',1)[0]+''
        irc.reply(txt, to=n, private=True)


    def _tellstatus(self, irc, table):
        """
        urnan
        """
        players = [p for p in self.game[table]['whiteplayers'].keys()]
        txt='The following faggots now need to pick their answer:'
        for p in players:    
            txt += '\x039 %s ' % (p)
            txt=txt.rsplit(' ',1)[0]+''
        irc.reply(txt, prefixNick=False, to=self.game[table]['channel'])
        for n in players:
            s = '%s: The question is %s Type %schoose "number" to select your answer.' % (n, self.game[table]['question'], conf.supybot.reply.whenAddressedBy.chars()[0])
            irc.reply(s, to=n, private=True)
            self._cards(irc, table, n)     


    def _cleanup(self, table):
        """
        urnan
        """
        self.game[table]['dealer']=''
        self.game[table]['whiteplayers']={}
        self.game[table]['turn']=''
        self.game[table]['question']=''
        self.game[table]['answers']={}
        self.game[table]['answerstwo']=[]
        self.game[table]['blackcards']=[]
        self.game[table]['picked']=[]
        for n in self.game[table]['whiteplayers'].keys():
            self.game[table]['whiteplayers'][n]['hand']=[]
            self.game[table]['whiteplayers'][n]['choice']=''


    def _getopentable(self):
        openslot=[i for i in range(len(self.game)) if not self.game[i].get('phase')]
        if len(openslot)==0:
            return None
        else:
            return openslot[0]

    def _getcurrenttables(self):
        slot=[i for i in range(len(self.game)) if self.game[i].get('phase')]
        return slot

    def _gettablefromnick(self, n):
        tables=self._getcurrenttables()
        if not tables: return None
        for table in tables:
            if n.lower() in map(lambda x:x.lower(), self.game[table]['players']):
                return table
        return None

    def _read_options(self, irc):
        network=irc.network.replace(' ','_')
        channel=irc.msg.args[0]
        #irc.reply('test: %s.%s.options' % (irc.network, irc.msg.args[0] ))
        
        f="%s%s.%s.options" % (self.dataPath, network, channel)
        if os.path.isfile(f):
            inputfile = open(f, "rb")
            self.channeloptions = pickle.load(inputfile)
            inputfile.close()
        else:
            # Use defaults
            channeloptions = {}
            channeloptions['allow_game']=False
            channeloptions['debug']=False
            channeloptions['use_queue']=True
            channeloptions['nplayers']=10
            channeloptions['maxbots']=9
        return

    def _write_options(self, irc):
        network=irc.network.replace(' ','_')
        channel=irc.msg.args[0]
        outputfile = open("%s%s.%s.options" % (self.dataPath, network, channel), "wb")
        pickle.dump(self.channeloptions, outputfile)
        outputfile.close()

    def doNick(self, irc, msg):
        oldNick = msg.nick
        newNick = msg.args[0]
        table=self._gettablefromnick(oldNick)
        if table == None:
            return
        else:
            s="Nick changes breaks this game coz cocks, a fix will be coming soon but until then, don't change nick. Start a new game."
            irc.reply(s, to=self.game[table]['channel'])
            self.game[table]={}
            self.game[table]['players']=[]
            self.game[table]['phase']=''
            irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
            return
        #if self.game[table]['phase']=='join':
        #    self.game[table]['players'][newNick]=self.game[table]['players'][oldNick]
        #    self.game[table]['players'].remove(oldNick)
        #    return
        #elif self.game[table]['phase']=='running':
        #    if oldNick == self.game[table]['dealer']:
        #        self.game[table]['dealer']=newNick
        #        self.game[table]['players'][newNick]=self.game[table]['players'][oldNick]
        #        self.game[table]['players'].remove(oldNick)
        #    else:
        #        self.game['whiteplayers'][newNick]=self.game[table]['whiteplayers'][oldNick]
        #        self.game[table]['whiteplayers'].remove(oldNick)
        #        self.game[table]['players'][newNick]=self.game[table]['players'][oldNick]
        #        self.game[table]['players'].remove(oldNick)

    def doQuit(self, irc, msg):
        nick=msg.nick
        table=self._gettablefromnick(nick)
        if table == None:
            return
        if self.game[table]['phase']=='join':
            self.game[table]['players'].remove(nick)
            if len(self.game[table]['players'])==0:
                self.game[table]={}
                self.game[table]['turn']=''
            return
        if self.game[table]['phase']=='running':
            self.game[table]['players'].remove(nick)
        else:
            irc.reply('There was a problem. Ending game.', prefixNick=False, to=self.game[table]['channel'])
            self.game[table]['players']={}
            self.game[table]['phase']=''
            irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
            return

        if len(self.game[table]['players'])<3:
            irc.reply('Not enough players left. Ending game.', prefixNick=False, to=self.game[table]['channel'])
            self.game[table]['players']={}
            self.game[table]['phase']=''
            irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
            
        else:
            if nick == self.game[table]['dealer']:
                if self.game[table]['turn']=='tellstatus' or self.game[table]['turn']=='picked':
                    irc.reply('%s left the game. Restarting round.' % nick, prefixNick=False)
                    txt='Given answers:'
                    players=self.game[table]['whiteplayers'].keys()
                    try:
                        for p in players:
                            ochoice=self.game[table]['whiteplayers'][p]['choice']
                            txt += '\x039 %s:\x034 %s ' % (p, ochoice)
                            txt=txt.rsplit(' ',1)[0]+''
                    except KeyError:
                        pass
                    irc.reply(txt, prefixNick=False, to=self.game[table]['channel'])
                    self._cleanup(table)
                    self._cah_begin(irc, table)
                else:
                    irc.reply('%s left the game. Restarting round.' % nick, prefixNick=False)
                    self._cleanup(table)
                    self._cah_begin(irc, table) 
            else:
                if not nick in self.game[table]['picked']:
                    self.game[table]['whiteplayers'][nick]={}
                    del self.game[table]['whiteplayers'][nick]
                    if len(self.game[table]['whiteplayers'].keys()) == len(self.game[table]['picked']):
                        random.shuffle(self.game[table]['answerstwo'])
                        self._picked(irc, table)
 

    def doPart(self, irc, msg):
        #self.log.info('doPart debug: msg.args[0]=%s, msg.args[1]=%s, msg.command=%s, msg.nick=%s' % (msg.args[0], msg.args[1], msg.command, msg.nick))
        nick=msg.nick
        table=self._gettablefromnick(nick)
        if table == None:
            return
        if msg.args[0] == self.game[table]['channel']:
            if self.game[table]['phase']=='join':
                self.game[table]['players'].remove(nick)
                if len(self.game[table]['players'])==0:
                    self.game[table]={}
                    self.game[table]['turn']=''
                return
            if self.game[table]['phase']=='running':
                self.game[table]['players'].remove(nick)
            else:
                irc.reply('There was a problem. Ending game.', prefixNick=False, to=self.game[table]['channel'])
                self.game[table]['players']={}
                self.game[table]['phase']=''
                irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
                return
            if len(self.game[table]['players'])<3:
                irc.reply('Not enough players left. Ending game.', prefixNick=False, to=self.game[table]['channel'])
                self.game[table]['players']={}
                self.game[table]['phase']=''
                irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
            
            else:
                if nick == self.game[table]['dealer']:
                    if self.game[table]['turn']=='tellstatus' or self.game[table]['turn']=='picked':
                        irc.reply('%s left the game. Restarting round.' % nick, prefixNick=False)
                        txt='Given answers:'
                        players=self.game[table]['whiteplayers'].keys()
                        try:
                            for p in players:
                                ochoice=self.game[table]['whiteplayers'][p]['choice']
                                txt += '\x039 %s:\x034 %s ' % (p, ochoice)
                                txt=txt.rsplit(' ',1)[0]+''
                        except KeyError:
                            pass
                        irc.reply(txt, prefixNick=False, to=self.game[table]['channel'])
                        self._cleanup(table)
                        self._cah_begin(irc, table)
                    else:
                        irc.reply('%s left the game. Restarting round.' % nick, prefixNick=False)
                        self._cleanup(table)
                        self._cah_begin(irc, table) 
                else:
                    if not nick in self.game[table]['picked']:
                        self.game[table]['whiteplayers'][nick]={}
                        del self.game[table]['whiteplayers'][nick]
                        if len(self.game[table]['whiteplayers'].keys()) == len(self.game[table]['picked']):
                            random.shuffle(self.game[table]['answerstwo'])
                            self._picked(irc, table)

    def doKick(self, irc, msg):
        (channel, nicks) = msg.args[:2]
        nicks=nicks.split(',')
        for nick in nicks:
            table=self._gettablefromnick(nick)
            if table!=None:
                if self.game[table]['phase']=='join':
                    self.game[table]['players'].remove(nick)
                    if len(self.game[table]['players'])==0:
                        self.game[table]={}
                        self.game[table]['turn']=''
                    return
                if self.game[table]['phase']=='running':    
                    self.game[table]['players'].remove(nick)
                if len(self.game[table]['players'])<3:
                    irc.reply('Not enough players left. Ending game.', prefixNick=False, to=self.game[table]['channel'])
                    self.game[table]['players']={}
                    self.game[table]['phase']=''
                    irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
                else:
                    if nick == self.game[table]['dealer']:
                        if self.game[table]['turn']=='tellstatus' or self.game[table]['turn']=='picked':
                            irc.reply('%s left the game. Restarting round.' % nick, prefixNick=False)
                            txt='Given answers:'
                            players=self.game[table]['whiteplayers'].keys()
                            try:
                                for p in players:
                                    ochoice=self.game[table]['whiteplayers'][p]['choice']
                                    txt += '\x039 %s:\x034 %s ' % (p, ochoice)
                                    txt=txt.rsplit(' ',1)[0]+''
                            except KeyError:
                                pass
                            irc.reply(txt, prefixNick=False, to=self.game[table]['channel'])
                            self._cleanup(table)
                            self._cah_begin(irc, table)
                        else:
                            irc.reply('%s left the game. Restarting round.' % nick, prefixNick=False)
                            self._cleanup(table)
                            self._cah_begin(irc, table) 
                    else:
                        if not nick in self.game[table]['picked']:
                            self.game[table]['whiteplayers'][nick]={}
                            del self.game[table]['whiteplayers'][nick]
                            if len(self.game[table]['whiteplayers'].keys()) == len(self.game[table]['picked']):
                                random.shuffle(self.game[table]['answerstwo'])
                                self._picked(irc, table)


    def rules(self, irc, msg, args):
        """
        Rules of Cards Against Humanity
        """
        irc.reply('A randomly chosen player decides on a question chosen from his hand of cards and will then decide the best answer from the other players who pick a funny/twisted/true/cocks/etc... card from their own hand. Simple, no?', prefixNick=False)
    rules=wrap(rules)

    def reset(self, irc, msg, args):
        """
        Resets all tables if something went wrong.
        """
        nick = msg.nick
        tables = self._getcurrenttables()
        if not ircdb.checkCapability(msg.nick, 'admin'):
            return

        no=0
        for table in tables:
            irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
            self._cleanup(table)
            self.game[table]={}
            self.game[table]['players']=[]
            self.game[table]['phase']=''
            n=no
            no=n+1
        if no==0:
            irc.reply('No games to reset.', prefixNick=False)
            return
        elif no==1:
            irc.reply('1 game has been reset.', prefixNick=False)
            return
        else:
            irc.reply('%d games has been reset' % no, prefixNick=False)
            return

    reset = wrap(reset)

    def score(self, irc, msg, args, text):
        """
        Find your or [nick]s total Wins
        """
        if text:
            nick=text
        else:
            nick=msg.nick
        self.tempscores={}
        f = open('/home/munchies/bot/plugins/CAH/scores.txt', 'r')
        line = f.readline()
        while line:
            (name, score) = line.split(' ')
            self.tempscores[name] = int(score.strip('\r\n'))
            line = f.readline()
        f.close()
        try:
            self.tempscores_lower = dict((k.lower(),v) for k,v in self.tempscores.items())
            score=self.tempscores_lower[nick.lower()]
            irc.reply('Total wins for %s: %d' % (nick, score), prefixNick=False)
        except KeyError:
            irc.reply('No entry found for %s.' % nick, prefixNick=False)
            return

    score = wrap(score, [optional('something')])

    def leave(self, irc, msg, args, text):
        """
        Leave the game
        """
        tables=self._getcurrenttables()
        if text:
            if not ircdb.checkCapability(msg.nick, 'admin'):
                return
            else:
                nick = text
        else:
            nick=msg.nick
        table=self._gettablefromnick(nick)
        if table == None:
            return
        if msg.args[0] == self.game[table]['channel']:
            irc.reply('Butthole.', prefixNick=False)
            if self.game[table]['phase']=='join':
                self.game[table]['players'].remove(nick)
                if len(self.game[table]['players'])==0:
                    self.game[table]={}
                    self.game[table]['turn']=''
                return
            if self.game[table]['phase']=='running':
                self.game[table]['players'].remove(nick)
            else:
                irc.reply('There was a problem. Ending game.', prefixNick=False, to=self.game[table]['channel'])
                self.game[table]['players']={}
                self.game[table]['phase']=''
                irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
                return
            if len(self.game[table]['players'])<3:
                irc.reply('Not enough players left. Ending game.', prefixNick=False, to=self.game[table]['channel'])
                self.game[table]['players']={}
                self.game[table]['phase']=''
                irc.sendMsg(ircmsgs.mode(self.game[table]['channel'], ["-Q"]))
            
            else:
                if nick == self.game[table]['dealer']:
                    if self.game[table]['turn']=='tellstatus' or self.game[table]['turn']=='picked':
                        irc.reply('%s left the game. Restarting round.' % nick, prefixNick=False)
                        txt='Given answers:'
                        players=self.game[table]['whiteplayers'].keys()
                        try:
                            for p in players:
                                ochoice=self.game[table]['whiteplayers'][p]['choice']
                                txt += '\x039 %s:\x034 %s ' % (p, ochoice)
                                txt=txt.rsplit(' ',1)[0]+''
                        except KeyError:
                            pass
                        irc.reply(txt, prefixNick=False, to=self.game[table]['channel'])
                        self._cleanup(table)
                        self._cah_begin(irc, table)
                    else:
                        irc.reply('%s left the game. Restarting round.' % nick, prefixNick=False)
                        self._cleanup(table)
                        self._cah_begin(irc, table)                        
                else:
                    if not nick in self.game[table]['picked']:
                        self.game[table]['whiteplayers'][nick]={}
                        del self.game[table]['whiteplayers'][nick]
                        if len(self.game[table]['whiteplayers'].keys()) == len(self.game[table]['picked']):
                            random.shuffle(self.game[table]['answerstwo'])
                            self._picked(irc, table)
    leave = wrap(leave, [optional('something')])

    def topscores(self, irc, msg, args):
        """
        Show the top scores
        """
        self.tscores={}
        f = open('/home/munchies/bot/plugins/CAH/scores.txt', 'r')
        line = f.readline()
        while line:
            (name, score) = line.split(' ')
            self.tscores[name] = int(score.strip('\r\n'))
            line = f.readline()
        f.close()
        self.topscores = sorted(self.tscores.iteritems(), key=lambda (k, v): (-v, k))[:3]
        for x in self.topscores:
            s = "{0}: {1}".format(*x)
            irc.reply(s)

    topscores = wrap(topscores)

    def faggots(self, irc, msg, args):
        """
        Who are you waiting for?
        """
        nick=msg.nick
        table=self._gettablefromnick(nick)
        if table==None:
            return
        if self.game[table]['turn']!='tellstatus':
            return
        else:
            txt='The following faggots need to choose an answer:'
            players=self.game[table]['whiteplayers'].keys()
            for p in players:
                if p in self.game[table]['picked']:
                    pass
                else:    
                    txt += '\x039 %s ' % (p)
                    txt=txt.rsplit(' ',1)[0]+''
            irc.reply(txt, prefixNick=False, to=self.game[table]['channel'])                    

    faggots = wrap(faggots)

Class = CAH



# vim:set shiftwidth=4 softtabstop=4 expandtab textwidth=79:
